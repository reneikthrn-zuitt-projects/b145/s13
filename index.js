//displays the message 
console.log("Hello from JS"); 

// this is a single-line commment 

/* multi-line commment
console.log("hello from JS");
console.log("hello from JS");
console.log("hello from JS");
console.log("hello from JS");
console.log("hello from JS");
*/

// ctrl + /
// console.log("end of comment"); 

console.log("example statement");

//syntax: console.log(value/message);

//[DECLARING VARIABLES]
// -> tell our devices that a variable name is created and ready to store data.

// syntax: let/const desiredVariableName;

let myVariable; 

//WHAT is a variable?
	// ==> It is used to contain/store data.

//Benefits of utilizing a variable?
    //This makes it it easier for us to associate information stored in our devices to actual names about the information.

 // an assignment operator (=) is used to assign/pass down values into a variable
let clientName = "Juan Dela Cruz";
let contactNumber = "09951446335";

//[PEEKING INSIDE A VARIABLE]
let greetings;

console.log(clientName); 
console.log(contactNumber); 
console.log(greetings);
let pangalan = "John Doe"; 
console.log(pangalan);
